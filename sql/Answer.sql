# 1.How many females and how many male passengers travelled for a minimum distance of 600 KM s?

select count(case when Gender = 'M' then 1 end) as COunt_of_Male_Passenger,
     count(case when Gender = 'F' then 1 end) as Count_Of_Female_Passenger
from passenger
where Distance >= 600;

 
# 2. Find the minimum ticket price for Sleeper Bus.

select min(price)
from price
where Bus_Type = 'Sleeper';

 
# 3. Select passenger names whose names start with character 'S'.

select * from passenger where Passenger_name Like "S%";

 
# 4.Calculate price charged for each passenger displaying Passenger name, Boarding City, Destination City, Bus_Type, Price in the output. 

select distinct(passenger.Passenger_Name) as Name,
	passenger.Boarding_City as Boarding_City,
	passenger.Destination_City as Destination_City,
	passenger.Bus_Type as Bus_Type,
	price.Price as Price
	from passenger, price
where passenger.Bus_Type = price.Bus_Type
	and passenger.Distance = price.Distance;


# 5. What is the passenger name and his/her ticket price who travelled in Sitting bus  for a distance of 1000 KMs.

select distinct(passenger.Passenger_Name) as Name,
	price.Price as Price
	from passenger, price
where passenger.Bus_Type = price.Bus_Type
	and passenger.Distance = price.Distance
	and passenger.Bus_Type = 'Sitting'
	and passenger.Distance = 1000;

 
# 6. What will be the Sitting and Sleeper bus charge for Pallavi to travel from Bangalore to Panaji?
# Distance between Bengaluru and panaji is 600 km

select (CASE WHEN price.Bus_Type = 'Sitting' THEN price.Price end) as Sitting_Price,
	(CASE WHEN price.Bus_Type = 'Sleeper' THEN price.Price end) as Sleeper_Price
	from  price, passenger
Where price.Distance = 600 and passenger.Passenger_name = 'Pallavi';
 

#  7.List the distances from the "Passenger" table which are unique (non-repeated distances) in descending order.

select distinct(Distance) as Distance from passenger Order By Distance DESC;


# 8. Display the passenger name and percentage of distance travelled by that passenger from the total distance travelled by all passengers without using user variables.

select passenger.Passenger_name,
	((passenger.Distance * 100)/td.Total_Distance) as Dist_Percentage
from passenger,
	(select sum(passenger.Distance) as Total_Distance from passenger)td
	order by passenger.Passenger_name;
 

# 9. Create a view to see all passengers who travelled in AC Bus.

CREATE OR REPLACE VIEW vw_passenger as select * from passenger where Category = 'AC';
select * from vw_passenger;

  
# 10. Create a stored procedure to find total passengers traveled using Sleeper buses.

Create procedure total_Passenger()
select count(Passenger_name) as Total from passenger where Bus_Type = 'Sleeper';

# Calling the procedure.

call total_Passenger;

 
# 11. Display 5 records at one time

select * from passenger limit 5