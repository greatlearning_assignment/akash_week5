Dear Participant,

Graded Assignment 5
Kindly go through the problem statement given below carefully and attempt the assignment and submit it on or before the due date.

Thanks,
Program Office.
# TravelOnMind

## Problem Statement

Travel on Mind is a online Portal which deals with bus booking system. The team of developers are trying to prepare the database and trying to perform few actions on that. we need to provide the solution.

## User Stories:-

1. How many females and how many male passengers travelled for a minimum distance of 600 KM s?
2. Find the minimum ticket price for Sleeper Bus. 
3. Select passenger names whose names start with character 'S' .
4. Calculate price charged for each passenger displaying Passenger name, Boarding City, Destination City, Bus_Type, Price in the output.
5. What is the passenger name and his/her ticket price who travelled in Sitting bus  for a distance of 1000 KMs. 
6. What will be the Sitting and Sleeper bus charge for Pallavi to travel from Bangalore to Panaji?
7. List the distances from the "Passenger" table which are unique (non-repeated distances) in descending order.
8. Display the passenger name and percentage of distance travelled by that passenger from the total distance travelled by all passengers without using user variables. 
9. Create a view to see all passengers who travelled in AC Bus.
10. Create a stored procedure to find total passengers traveled using Sleeper buses.
11. Display 5 records at one time
12. Create 1NF , 2Nf and 3NF normal form of the blow data:-
>> ![](images/data.png) 


### Instructions:-

1. The solution should be submitted into data folder in the given repo
2. To run the script please use source command.
    ex:- source G:/Assignment/Database/greadedassignment1/Mysql/Travel.sql;
3. Please submit the solution in .sql file.
4. Please make sure there should be no compile time error in the file. In case of compile time error 0 marks will be given.
5. The solution of Q12 should be submitted in the form pdf
6. There is no partial marking in this module.

## Boilerplate

Click here to get the boilerplate
https://gitlab.com/hcl45/assignments/week5


| Implementation        | MM          |
| ----------------------| ----------- |
| Point 1 to 5          | 1           |
| Point 6 to 11         | 2           |
| Point 12              | 3           |
